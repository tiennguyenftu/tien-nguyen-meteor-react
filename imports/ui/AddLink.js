import React, {Component} from 'react';
import {Meteor} from 'meteor/meteor';
import Modal from 'react-modal';

export default class Link extends Component {
  constructor(props) {
    super(props);

    this.state = {
      url: '',
      isOpen: false,
      error: ''
    };
  }

  onSubmit(e) {
    e.preventDefault();

    const {url} = this.state;
    Meteor.call('links.insert', url, (err) => {
      if (!err) {
        this.handleModalClose();
      } else {
        this.setState({error: err.reason})
      }
    });
  }

  onChange(e) {
    this.setState({
      url: e.target.value
    });
  }

  handleModalClose() {
    this.setState({isOpen: false, url: '', error: ''});
  }

  render() {
    const {url, isOpen, error} = this.state;
    return (
      <div>
        <button className="button" onClick={() => this.setState({isOpen: true})}>+ Add Link</button>
        <Modal
          isOpen={isOpen}
          contentLabel="Add Link"
          onAfterOpen={() => this.refs.url.focus()}
          onRequestClose={this.handleModalClose.bind(this)}
          className="boxed-view__box"
          overlayClassName="boxed-view boxed-view--modal"
        >
          <h1>Add Link</h1>
          {error ? <p>{error}</p> : null}
          <form onSubmit={this.onSubmit.bind(this)} className="boxed-view__form">
            <input
              type="text"
              placeholder="URL"
              ref="url"
              value={url}
              onChange={this.onChange.bind(this)}
            />
            <button className="button">Add Link</button>
            <button type="button" className="button button--secondary" onClick={this.handleModalClose.bind(this)}>Cancel</button>
          </form>
        </Modal>
      </div>
    )
  }
}