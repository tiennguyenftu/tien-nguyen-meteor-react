import React from 'react';

import PrivateHeader from './PrivateHeader';
import LinksListFilter from './LinksListFilter';
import LinksList from './LinksList';
import AddLink from './AddLink';

export default () => (
  <div>
    <PrivateHeader title="Your Links"/>
    <div className="page-content">
      <LinksListFilter/>
      <AddLink/>
      <LinksList/>
    </div>
  </div>
);