import React, {Component} from 'react';
import Clipboard from 'clipboard';
import PropTypes from 'prop-types';
import moment from 'moment';

export default class LinksListItem extends Component {
  constructor(props) {
    super(props);

    this.state = {
      justCopied: false
    }
  }

  componentDidMount() {
    this.clipboard = new Clipboard(this.refs.copy);

    this.clipboard.on('success', () => {
      this.setState({justCopied: true});
      setTimeout(() => {
        this.setState({justCopied: false});
      }, 1000);
    }).on('error', () => {
      this.setState({justCopied: false});
    });
  }

  componentWillUnmount() {
    this.clipboard.destroy();
  }

  setVisibility = () => {
    const {_id, visible} = this.props;
    Meteor.call('links.setVisibility', _id, !visible);
  };

  renderStats() {
    const {visitedCount, lastVisitedAt} = this.props;
    const visitMessage = visitedCount === 1 ? 'visit' : 'visits';
    let visitedMessage = null;
    if (typeof lastVisitedAt === 'number') {
      visitedMessage = `(visited ${moment(lastVisitedAt).fromNow()})`;
    }
    return <p className="item__message">{visitedCount} {visitMessage} {visitedMessage}</p>
  }

  render() {
    const {url, shortUrl, visible} = this.props;
    const {justCopied} = this.state;
    return (
      <div className="item">
        <h2>{url}</h2>
        <p className="item__message">{shortUrl}</p>
        {this.renderStats()}
        <a className="button button--pill button--link" href={shortUrl} target="_blank">Visit</a>
        <button className="button button--pill" ref="copy" data-clipboard-text={shortUrl}>{justCopied ? 'Copied' : 'Copy'}</button>
        <button className="button button--pill" onClick={this.setVisibility}>{visible ? 'Hide' : 'Unhide'}</button>
      </div>
    )
  }
}

LinksListItem.propTypes = {
  _id: PropTypes.string.isRequired,
  userId: PropTypes.string.isRequired,
  url: PropTypes.string.isRequired,
  shortUrl: PropTypes.string.isRequired,
  visible: PropTypes.bool.isRequired,
  visitedCount: PropTypes.number.isRequired,
  lastVisitedAt: PropTypes.number
};
